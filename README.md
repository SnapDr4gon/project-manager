# Proyecto SCRUM

Universidad Autónoma de Chihuahua.

Ingeniería en ciencias de la computación.

WEB PLATFORMS 7CC2.

Jared Flores Martinez (353391).

Gabriel Jesús Bustillos Fierro (353267).

Gabriel Isaí Prieto Sánez (353297).

Axel Ricardo Moncloa Muro (348752).

Los siguientes scripts son los primeros resultados de nuestra primera aplicación de un scrum.

### Prerequisitos

Nota:

Hasta el momento no es necesario que tengas instalado node debido que solamente es una parte del proyecto sin embargo puedes tomarlo a consideración para el futuro.

Deberás contar con node para ejecutar la aplicación lo puedes verificar con:

```
node -v
```

### Instalación en local

Clona el proyecto en la carpeta de tu preferencia de la siguiente manera:

```json
git clone https://gitlab.com/SnapDr4gon/project-manager.git
```

Después de haber clonado el repositorio te arrojará una serie de archivos.

Solamente es una parte de nuestro proyecto por lo tanto no funcionará.

Si deseas editarlos o realizar alguna modificación lo puedes hacer desde tu editor de texto preferido,en mi caso utilizo Visual Studio Code el cual te recomiendo (https://code.visualstudio.com/).

## Diagrama De Clases

![Diagrama de clases](./diagrams/projectManager.png)

## Diagramas De Secuencia

- Creacion de proyecto

![Crear proyecto](./diagrams/crearProyecto.png)

- Asignar miembros a un proyecto

![Asignar miembros](./diagrams/asignarMiembros.png)

- Login

![Login](./diagrams/registro.png)

- Añadir habilidades

![Añadir habilidades](./diagrams/addSkill.png)

- Añadir tarjeta

![Añadir tarjeta](./diagrams/addCard.png)

- Modificar tarjeta

![Modificar tarjeta](./diagrams/modifyCard.png)

- Mover tarjeta

![Mover tarjeta](./diagrams/moveCard.png)

- Eliminar tarjeta

![Eliminar tarjeta](./diagrams/deleteCard.png)

- Ver graficos

![Visualizar graficos](./diagrams/seeGraphics.png)

## Link de la imagen de docker

https://hub.docker.com/repository/docker/gabop0911/project-manager/general

# ¿Dudas?, ¿Sugerencias?

Si quieres que realice alguna mejora general, abre un "issue" aquí en github. Lo modificaremos lo antes posible.
